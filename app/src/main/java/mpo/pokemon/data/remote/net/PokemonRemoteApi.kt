package mpo.pokemon.data.remote.net

import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.data.remote.model.TotalPokemonEntry
import retrofit2.Call
import retrofit2.http.*

interface PokemonRemoteApi {

    @GET("pokemon/{value}")
    fun getPokemonByValue(@Path("value") value: String): Call<PokemonEntry>

    @GET("pokemon/{value}")
    fun getPokemonDiscoverByValue(@Path("value") value: Int): Call<PokemonEntry>

    @GET("pokemon/")
    fun getAllCountRemotePokemons(): Call<TotalPokemonEntry>
}