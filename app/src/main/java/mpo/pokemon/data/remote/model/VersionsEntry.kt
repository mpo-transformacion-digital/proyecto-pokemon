package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class VersionsEntry (
    @SerializedName("generation-i")
    val generationI: GenerationIEntry,

    @SerializedName("generation-ii")
    val generationIi: GenerationIiEntry,

    @SerializedName("generation-iii")
    val generationIii: GenerationIiiEntry,

    @SerializedName("generation-iv")
    val generationIv: GenerationIvEntry,

    @SerializedName("generation-v")
    val generationV: GenerationVEntry,

    @SerializedName("generation-vi")
    val generationVi: Map<String, HomeEntry>,

    @SerializedName("generation-vii")
    val generationVii: GenerationViiEntry,

    @SerializedName("generation-viii")
    val generationViii: GenerationViiiEntry
)