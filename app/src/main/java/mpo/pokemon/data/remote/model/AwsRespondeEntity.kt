package mpo.pokemon.data.remote.model

data class AwsRespondeEntity (
    val code: Long,
    val desc: String,
    val response: String
)