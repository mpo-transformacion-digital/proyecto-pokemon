package mpo.pokemon.data.remote

import androidx.viewbinding.BuildConfig
import mpo.pokemon.data.remote.net.PokemonAWSRemoteApi
import mpo.pokemon.data.remote.net.PokemonRemoteApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private const val BASE_URL = "https://pokeapi.co/api/v2/"
    private const val BASE_URL_AWS = "https://94qsi4cb05.execute-api.us-east-1.amazonaws.com/test/"

    private fun getRetrofit() : Retrofit {

        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        if (BuildConfig.BUILD_TYPE == "debug") {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
            retrofitBuilder.client(client)
        }

        return retrofitBuilder.build()

    }

    private fun getAwsRetrofit() : Retrofit {

        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(BASE_URL_AWS)
            .addConverterFactory(GsonConverterFactory.create())

        if (BuildConfig.BUILD_TYPE == "debug") {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
            retrofitBuilder.client(client)
        }

        return retrofitBuilder.build()

    }

    val pokemonRemoteApi: PokemonRemoteApi = getRetrofit().create(PokemonRemoteApi::class.java)
    val pokemonAWSRemoteApi: PokemonAWSRemoteApi = getAwsRetrofit().create(PokemonAWSRemoteApi::class.java)

}