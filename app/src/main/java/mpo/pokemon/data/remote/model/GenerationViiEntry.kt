package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class GenerationViiEntry (
    val icons: DreamWorldEntry,

    @SerializedName("ultra-sun-ultra-moon")
    val ultraSunUltraMoon: HomeEntry
)