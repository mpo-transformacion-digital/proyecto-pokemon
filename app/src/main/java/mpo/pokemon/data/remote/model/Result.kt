package mpo.pokemon.data.remote.model

data class Result (
    val name: String,
    val url: String
)
