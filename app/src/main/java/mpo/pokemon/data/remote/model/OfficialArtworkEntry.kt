package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class OfficialArtworkEntry (
    @SerializedName("front_default")
    val frontDefault: String
)