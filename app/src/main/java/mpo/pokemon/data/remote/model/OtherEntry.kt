package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class OtherEntry (
    @SerializedName("dream_world")
    val dreamWorld: DreamWorldEntry,

    val home: HomeEntry,

    @SerializedName("official-artwork")
    val officialArtwork: OfficialArtworkEntry
)