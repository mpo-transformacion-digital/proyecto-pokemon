package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class VersionGroupDetailEntry (
    @SerializedName("level_learned_at")
    val levelLearnedAt: Long,

    @SerializedName("move_learn_method")
    val moveLearnMethod: SpeciesEntry,

    @SerializedName("version_group")
    val versionGroup: SpeciesEntry
)