package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class GenerationIEntry (
    @SerializedName("red-blue")
    val redBlue: RedBlueEntry,

    val yellow: RedBlueEntry
)