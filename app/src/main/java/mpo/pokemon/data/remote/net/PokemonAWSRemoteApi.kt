package mpo.pokemon.data.remote.net

import mpo.pokemon.data.remote.model.AwsRespondeEntity
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface PokemonAWSRemoteApi {

    @POST("effectiveness")
    fun getPokemonEffectiveness(@Body requestBody : RequestBody): Call<AwsRespondeEntity>
}