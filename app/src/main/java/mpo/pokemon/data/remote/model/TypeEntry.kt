package mpo.pokemon.data.remote.model

data class TypeEntry (
    val slot: Long,
    val type: SpeciesEntry
)