package mpo.pokemon.data.remote.model

data class SpeciesEntry (
    val name: String,
    val url: String
)