package mpo.pokemon.data.remote.source

import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.data.remote.model.TotalPokemonEntry
import mpo.pokemon.data.remote.model.toPokemonGlobal
import mpo.pokemon.data.remote.net.PokemonRemoteApi
import mpo.pokemon.data.repo.DiscoveryRepository
import mpo.pokemon.data.repo.SearchRepository
import mpo.pokemon.global.model.PokemonGlobalModel
import retrofit2.await

class PokemonRemoteDataSource (private val pokemonRemoteApi: PokemonRemoteApi): SearchRepository, DiscoveryRepository {

    override suspend fun search(value: String): PokemonEntry {
        return pokemonRemoteApi.getPokemonByValue(value).await()
    }

    override suspend fun getAllPokemonCount(): TotalPokemonEntry {
        return pokemonRemoteApi.getAllCountRemotePokemons().await()
    }

    override suspend fun searchDiscovery(value: Int): PokemonGlobalModel {
        var listGlobalPokemon = pokemonRemoteApi.getPokemonDiscoverByValue(value).await().toPokemonGlobal()//.map{it.toPokemonGlobal()}
        return listGlobalPokemon
    }

}


