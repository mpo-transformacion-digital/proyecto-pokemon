package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class EmeraldEntry (
    @SerializedName("front_default")
    val frontDefault: String,

    @SerializedName("front_shiny")
    val frontShiny: String
)