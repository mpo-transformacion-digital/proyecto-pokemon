package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class GenerationIiiEntry (
    val emerald: EmeraldEntry,

    @SerializedName("firered-leafgreen")
    val fireredLeafgreen: CrystalEntry,

    @SerializedName("ruby-sapphire")
    val rubySapphire: CrystalEntry
)