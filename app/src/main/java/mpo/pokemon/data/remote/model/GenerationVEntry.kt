package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class GenerationVEntry (
    @SerializedName("black-white")
    val blackWhite: SpritesEntry
)