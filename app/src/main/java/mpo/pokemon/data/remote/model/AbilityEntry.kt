package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class AbilityEntry (
    val ability: SpeciesEntry,

    @SerializedName("is_hidden")
    val isHidden: Boolean,

    val slot: Long
)