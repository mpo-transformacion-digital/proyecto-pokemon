package mpo.pokemon.data.remote.model

import java.io.Serializable

data class TotalPokemonEntry (
    val count: Long,
    val next: String,
    val previous: Any? = null,
    val results: List<Result>
) : Serializable

