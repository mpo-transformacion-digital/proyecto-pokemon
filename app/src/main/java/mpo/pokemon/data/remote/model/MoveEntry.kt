package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class MoveEntry (
    val move: SpeciesEntry,

    @SerializedName("version_group_details")
    val versionGroupDetails: List<VersionGroupDetailEntry>
)