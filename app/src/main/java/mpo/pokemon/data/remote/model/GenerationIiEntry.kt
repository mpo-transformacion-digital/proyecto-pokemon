package mpo.pokemon.data.remote.model

data class GenerationIiEntry (
    val crystal: CrystalEntry,
    val gold: CrystalEntry,
    val silver: CrystalEntry
)