package mpo.pokemon.data.remote.source

import mpo.pokemon.data.remote.model.AwsRespondeEntity
import mpo.pokemon.data.remote.net.PokemonAWSRemoteApi
import mpo.pokemon.data.repo.CalculatorRepository
import okhttp3.RequestBody
import retrofit2.await

class PokemonAwsRemoteDataSource (private val pokemonAwsRemoteApi: PokemonAWSRemoteApi): CalculatorRepository {

    override suspend fun getPokemonEffectiveness(requestBody : RequestBody): AwsRespondeEntity {
        return pokemonAwsRemoteApi.getPokemonEffectiveness(requestBody).await()
    }

}


