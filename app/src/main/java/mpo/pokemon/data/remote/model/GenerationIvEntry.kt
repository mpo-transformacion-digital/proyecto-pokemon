package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class GenerationIvEntry (
    @SerializedName("diamond-pearl")
    val diamondPearl: SpritesEntry,

    @SerializedName("heartgold-soulsilver")
    val heartgoldSoulsilver: SpritesEntry,

    val platinum: SpritesEntry
)