package mpo.pokemon.data.remote.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import mpo.pokemon.global.model.PokemonGlobalModel
import java.io.Serializable

data class PokemonEntry (

    val abilities: List<AbilityEntry>,

    @SerializedName("base_experience")
    val baseExperience: Long,

    val forms: List<SpeciesEntry>,

    @SerializedName("game_indices")
    val gameIndices: List<GameIndexEntry>,

    val height: Long,

    @SerializedName("held_items")
    val heldItems: List<Any?>,

    val id: Long,

    @SerializedName("is_default")
    val isDefault: Boolean,

    @SerializedName("location_area_encounters")
    val locationAreaEncounters: String,

    val moves: List<MoveEntry>,
    val name: String,
    val order: Long,

    @SerializedName("past_types")
    val pastTypes: List<Any?>,

    val species: SpeciesEntry,
    val sprites: SpritesEntry,
    val stats: List<StatEntry>,
    val types: List<TypeEntry>,
    val weight: Long
) : Serializable {
    fun idToString() : String{
        return id.toString()
    }
    /*public fun toJson() = klaxon.toJsonString(this)

    companion object {
        public fun fromJson(json: String) = klaxon.parse<PokemonEntry>(json)
    }*/
}

var gson = Gson()
fun PokemonEntry.toPokemonGlobal() = PokemonGlobalModel(
    idToString(),
    name,
    types[0].type.name,
    sprites.other!!.officialArtwork.frontDefault,
    stats[0].baseStat,
    stats[1].baseStat,
    stats[2].baseStat,
    stats[3].baseStat,
    stats[4].baseStat,
    stats[5].baseStat,
    gson.toJson(this)
)