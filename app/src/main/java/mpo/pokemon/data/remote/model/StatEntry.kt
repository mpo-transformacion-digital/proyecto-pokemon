package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class StatEntry (
    @SerializedName("base_stat")
    val baseStat: Long,

    val effort: Long,
    val stat: SpeciesEntry
) {
    fun baseStatToString() : String {
        return baseStat.toString()
    }
}