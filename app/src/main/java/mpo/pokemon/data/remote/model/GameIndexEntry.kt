package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class GameIndexEntry (
    @SerializedName("game_index")
    val gameIndex: Long,

    val version: SpeciesEntry
)