package mpo.pokemon.data.remote.model

import com.google.gson.annotations.SerializedName

data class DreamWorldEntry (
    @SerializedName("front_default")
    val frontDefault: String,

    @SerializedName("front_female")
    val frontFemale: Any? = null
)