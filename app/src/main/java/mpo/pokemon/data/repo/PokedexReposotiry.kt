package mpo.pokemon.data.repo

import androidx.lifecycle.LiveData
import mpo.pokemon.global.model.PokemonGlobalModel

interface PokedexReposotiry {
    suspend fun ldbsearch(pokemonId: String): List<PokemonGlobalModel>
    suspend fun add(pokemon: PokemonGlobalModel) : Long
    suspend fun getAllPokemonsFromPokedex() : LiveData<List<PokemonGlobalModel>>
    suspend fun getPokemonsFromPokedexDb(query: String) : LiveData<List<PokemonGlobalModel>>
    suspend fun deleteFromPokedex(id: String)
}