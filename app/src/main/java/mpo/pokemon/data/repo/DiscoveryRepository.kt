package mpo.pokemon.data.repo

import mpo.pokemon.data.remote.model.TotalPokemonEntry
import mpo.pokemon.global.model.PokemonGlobalModel

interface DiscoveryRepository {

    suspend fun getAllPokemonCount() : TotalPokemonEntry?
    suspend fun searchDiscovery(value : Int) : PokemonGlobalModel?
}