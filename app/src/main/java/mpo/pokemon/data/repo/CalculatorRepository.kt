package mpo.pokemon.data.repo

import mpo.pokemon.data.remote.model.AwsRespondeEntity
import okhttp3.RequestBody

interface CalculatorRepository {

    suspend fun getPokemonEffectiveness(requestBody : RequestBody) : AwsRespondeEntity?
}