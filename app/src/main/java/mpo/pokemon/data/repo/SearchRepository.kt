package mpo.pokemon.data.repo

import mpo.pokemon.data.remote.model.PokemonEntry

interface SearchRepository {

    suspend fun search(value : String) : PokemonEntry?

}