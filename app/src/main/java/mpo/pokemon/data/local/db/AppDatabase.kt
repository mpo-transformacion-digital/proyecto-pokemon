package mpo.pokemon.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import mpo.pokemon.data.local.model.PokedexEntity
import mpo.pokemon.data.remote.model.PokemonEntry

const val DATABASE_VERSION = 1
const val TABLE_POKEDEX = "pokedex"
const val DATABASE_NAME = "pokemon.sqlite"

@Database(
    entities = [PokedexEntity::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun pokedexDao(): PokedexDAO
}