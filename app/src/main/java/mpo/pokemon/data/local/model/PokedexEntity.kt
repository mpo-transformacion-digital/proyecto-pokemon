package mpo.pokemon.data.local.model

import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import mpo.pokemon.data.local.db.TABLE_POKEDEX
import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.global.model.PokemonGlobalModel

@Entity(tableName = TABLE_POKEDEX)
data class PokedexEntity (
    @PrimaryKey val pokemonId: String,
    val name: String,
    val type: String,
    @ColumnInfo(name = "image")
    val image: String,
    val hp: Long,
    val attack: Long,
    val defense: Long,
    val specialAttack: Long,
    val specialDefense: Long,
    val speed: Long,
    val pokemonEntry: String
        )

fun PokedexEntity.toPokemonFromPokedex() = PokemonGlobalModel(
    pokemonId,
    name,
    type,
    image,
    hp,
    attack,
    defense,
    specialAttack,
    specialDefense,
    speed,
    pokemonEntry
)