package mpo.pokemon.data.local.`interface`

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import mpo.pokemon.data.local.db.PokedexDAO
import mpo.pokemon.data.local.model.toPokemonFromPokedex
import mpo.pokemon.data.repo.PokedexReposotiry
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.global.model.toPokedexEntity

class PokedexDataSource(
    private val pokedexDao: PokedexDAO
) : PokedexReposotiry {

    override suspend fun ldbsearch(pokemonId: String): List<PokemonGlobalModel> {
        return pokedexDao.getPokemonFromPokedex(pokemonId).map { it.toPokemonFromPokedex() }
    }

    override suspend fun add(pokemon: PokemonGlobalModel): Long {
        return pokedexDao.save(pokemon.toPokedexEntity())
    }

    override suspend fun getAllPokemonsFromPokedex(): LiveData<List<PokemonGlobalModel>> {
        return MutableLiveData(pokedexDao.getAllPokemonsFromPokedex().map { it.toPokemonFromPokedex() })
    }

    override suspend fun getPokemonsFromPokedexDb(query: String): LiveData<List<PokemonGlobalModel>> {
        return MutableLiveData(pokedexDao.getPokemonsFromPokedexDB(query).map { it.toPokemonFromPokedex() })
    }

    override suspend fun deleteFromPokedex(id: String){
        return pokedexDao.deletePokemonFromPokedexDB(id)
    }
}