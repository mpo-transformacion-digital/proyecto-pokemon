package mpo.pokemon.data.local.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mpo.pokemon.data.local.model.PokedexEntity

@Dao
interface PokedexDAO {
    @Query("SELECT * FROM $TABLE_POKEDEX where pokemonId LIKE :pokemonId")
    suspend fun getPokemonFromPokedex(pokemonId: String): List<PokedexEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(pokemon : PokedexEntity) : Long

    @Query("SELECT * FROM $TABLE_POKEDEX")
    suspend fun getAllPokemonsFromPokedex() : List<PokedexEntity>

    @Query("SELECT * FROM $TABLE_POKEDEX where name LIKE :query")
    suspend fun getPokemonsFromPokedexDB(query: String): List<PokedexEntity>

    @Query("DELETE FROM $TABLE_POKEDEX where pokemonId LIKE :query")
    suspend fun deletePokemonFromPokedexDB(query: String)

}