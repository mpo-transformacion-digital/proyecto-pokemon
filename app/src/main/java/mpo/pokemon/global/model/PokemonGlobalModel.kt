package mpo.pokemon.global.model

import mpo.pokemon.data.local.model.PokedexEntity
import java.io.Serializable


class PokemonGlobalModel(
    var pokemonId: String,
    var pokemonName : String,
    var pokemonType : String,
    var pokemonImage : String,
    val hp: Long,
    val attack: Long,
    val defense: Long,
    val specialAttack: Long,
    val specialDefense: Long,
    val speed: Long,
    val pokemonEntry: String
    ) : Serializable

fun PokemonGlobalModel.toPokemon() = PokemonGlobalModel(
    pokemonId,
    pokemonName,
    pokemonType,
    pokemonImage,
    hp,
    attack,
    defense,
    specialAttack,
    specialDefense,
    speed,
    pokemonEntry
)

fun PokemonGlobalModel.toPokedexEntity() = PokedexEntity(
    pokemonId,
    pokemonName,
    pokemonType,
    pokemonImage,
    hp,
    attack,
    defense,
    specialAttack,
    specialDefense,
    speed,
    pokemonEntry
)