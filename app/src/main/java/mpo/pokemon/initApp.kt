package mpo.pokemon

import android.app.Application
import mpo.pokemon.data.local.db.DatabaseManager

class initApp : Application() {

    override fun onCreate() {
        DatabaseManager.instance.initializeDb(applicationContext)
        super.onCreate()
    }
}