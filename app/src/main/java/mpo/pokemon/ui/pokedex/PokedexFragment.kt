package mpo.pokemon.ui.pokedex

import android.os.Bundle
import android.view.*
import android.widget.ScrollView
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import kotlinx.coroutines.launch
import mpo.pokemon.R
import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.databinding.PokedexFragmentBinding
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback

class PokedexFragment : Fragment(), PokemonListCallback, SearchView.OnQueryTextListener{

    companion object {
        fun newInstance() = PokedexFragment()
    }

    private lateinit var viewModel: PokedexViewModel
    private lateinit var pokedexList: RecyclerView
    private lateinit var _binding: PokedexFragmentBinding
    private lateinit var noDataPokedex: ScrollView
    private lateinit var dataPokedex: ScrollView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val searchOptions = menu.findItem(R.id.searchmenu)
        val searchView = searchOptions.actionView as SearchView
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(this)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.pokedex_fragment, container, false)
        pokedexList = rootView.findViewById(R.id.recyclerViewPokedex)
        val pokedexFactory = PokedexViewModel.PokedexFactory()
        viewModel = ViewModelProvider(this, pokedexFactory)[PokedexViewModel::class.java]
        noDataPokedex = rootView.findViewById(R.id.scrollNoPokedexData)
        dataPokedex = rootView.findViewById(R.id.scrollPokedexData)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.pokemonsPokedex.observe(viewLifecycleOwner) { pokemons ->
            var adapter = pokedexList.adapter
            if (adapter == null) {
                adapter = PokedexAdapter(pokemons, this)
                pokedexList.adapter = adapter
                if(pokemons.isNotEmpty()){
                    noDataPokedex.visibility = View.GONE
                    dataPokedex.visibility = View.VISIBLE
                    (pokedexList.adapter as PokedexAdapter).setPokemons(pokemons)
                }else{
                    noDataPokedex.visibility = View.VISIBLE
                    dataPokedex.visibility = View.GONE
                }
            } else {
                (pokedexList.adapter as PokedexAdapter).setPokemons(pokemons)
            }
        }
        viewModel.getAllPokemosFromDb()
    }

    override fun onClick(pokemon: PokemonGlobalModel) {
        val pokemonEntry = Gson().fromJson(pokemon.pokemonEntry, PokemonEntry::class.java)
        val directions = PokedexFragmentDirections.actionNavigationPokedexToPokemonDetailFragment(pokemonEntry,2)
        directions.arguments.putSerializable("pokemon_arg", pokemonEntry)
        directions.arguments.putSerializable("screen", 2)
        NavHostFragment.findNavController(this).navigate(directions)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(filter: String?): Boolean {
            if(filter!!.isNotEmpty()){
                lifecycleScope.launch {
                    viewModel.getPokemonFromDb(filter)
                    pokedexList.adapter = null
                }
            }else{
                lifecycleScope.launch {
                    viewModel.getAllPokemosFromDb()
                    pokedexList.adapter = null
                }
            }
        return false
    }


}