package mpo.pokemon.ui.pokedex

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mpo.pokemon.databinding.ItemPokedexBinding
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback
import mpo.pokemon.util.UtilBackgroundPokemonType

class PokedexAdapter (
    private var pokedex: List<PokemonGlobalModel>,
    private val callback: PokemonListCallback
) :
    RecyclerView.Adapter<PokedexAdapter.PokedexViewHolder>() {

    lateinit var allPokemons : List<PokemonGlobalModel>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokedexViewHolder {
        val binding = ItemPokedexBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PokedexViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: PokedexViewHolder, position: Int) {
        val currentPokemon = pokedex[position]
        holder.binding.pokedex = currentPokemon
        holder.bind(currentPokemon)
    }

    override fun getItemCount(): Int = pokedex.size
    fun setPokemons(newPokemons: List<PokemonGlobalModel>) {
        pokedex = newPokemons
        notifyDataSetChanged()
    }

    class PokedexViewHolder(
        val binding: ItemPokedexBinding,
        private val callback: PokemonListCallback
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item : PokemonGlobalModel){
            UtilBackgroundPokemonType().setBackgroundByPokemonType(item.pokemonType, binding.pokemonCard, binding.root.context)
        }

        init {
            binding.root.setOnClickListener {
                binding.pokedex?.let { pokemon ->
                    callback.onClick(pokemon)
                }
            }
        }
    }
}