package mpo.pokemon.ui.pokedex

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import mpo.pokemon.data.local.`interface`.PokedexDataSource
import mpo.pokemon.data.local.db.DatabaseManager
import mpo.pokemon.data.repo.PokedexReposotiry
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.ObservableViewModel
import java.lang.Exception

class PokedexViewModel(private val pokedexRepository: PokedexReposotiry) : ObservableViewModel(){

    val pokemonsPokedex = MutableLiveData<List<PokemonGlobalModel>>()

    fun getAllPokemosFromDb() {
        viewModelScope.launch {
            pokemonsPokedex.value = pokedexRepository.getAllPokemonsFromPokedex().value
        }
    }

    fun getPokemonFromDb(query : String) {
        viewModelScope.launch {
            pokemonsPokedex.value = pokedexRepository.getPokemonsFromPokedexDb("%${query}%").value
        }
    }

   class PokedexFactory: ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PokedexViewModel::class.java)){
                return PokedexViewModel(
                    pokedexRepository = PokedexDataSource(DatabaseManager.instance.database.pokedexDao()),
                ) as T
            }
            throw Exception("No class type supported")
        }
    }

}