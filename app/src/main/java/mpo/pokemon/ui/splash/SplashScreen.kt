package mpo.pokemon.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import mpo.pokemon.MainActivity
import mpo.pokemon.R

class   SplashScreen : AppCompatActivity() {

    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        val factory = SplashScreenViewModel.SplashScreenFactory()
        viewModel = ViewModelProvider(this, factory)[SplashScreenViewModel::class.java]

        viewModel.shouldStartMainScreen.observe(this) { shouldStart ->
            if (shouldStart) {
                startMainActivity()
                finish()
            }
        }

    }

    private fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}