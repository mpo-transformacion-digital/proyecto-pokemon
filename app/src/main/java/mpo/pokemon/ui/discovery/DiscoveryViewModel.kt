package mpo.pokemon.ui.discovery

import androidx.lifecycle.*
import mpo.pokemon.data.remote.RetrofitBuilder
import mpo.pokemon.data.remote.model.TotalPokemonEntry
import mpo.pokemon.data.remote.source.PokemonRemoteDataSource
import mpo.pokemon.data.repo.DiscoveryRepository
import mpo.pokemon.global.model.PokemonGlobalModel
import java.lang.Exception
import kotlin.random.Random

class DiscoveryViewModel(val discoveryRepo: DiscoveryRepository) : ViewModel() {

    val totalPokemon = MutableLiveData<TotalPokemonEntry>()
    val globalPokemonEntry = MutableLiveData<List<PokemonGlobalModel>>()
    var listGlobalPokemon = MutableLiveData<PokemonGlobalModel>()
    private val listTemp = ArrayList<PokemonGlobalModel>()

   suspend fun getCountPokemons(){
        totalPokemon.value = discoveryRepo.getAllPokemonCount()
        getPokemonByRandomeValue(totalPokemon.value!!.count.toInt())
    }

    //The API SHOWS A WRONG RESULT, I WASTE A LOT OF TIME HERE, THE API SHOWS 1118 POKEMON, AND JUST HAVE 889
    private suspend fun getPokemonByRandomeValue(value : Int){
        val randomValues = List(10) { Random.nextInt(0, 889) }
        val itr = randomValues.listIterator()
        while (itr.hasNext()){
            listGlobalPokemon.value = discoveryRepo.searchDiscovery(itr.next())
            listTemp.add(listGlobalPokemon.value!!)
            globalPokemonEntry.value = listTemp
        }
    }


    class DiscoveryViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DiscoveryViewModel::class.java)) {
                return DiscoveryViewModel(
                    discoveryRepo = PokemonRemoteDataSource(RetrofitBuilder.pokemonRemoteApi)
                ) as T
            }
            throw Exception("Class type not supported.")
        }
    }
}