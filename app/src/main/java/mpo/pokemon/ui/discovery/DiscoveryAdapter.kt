package mpo.pokemon.ui.discovery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mpo.pokemon.databinding.ItemPokedexBinding
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback
import mpo.pokemon.util.UtilBackgroundPokemonType

class DiscoveryAdapter(
    private var pokemons: List<PokemonGlobalModel>,
    private val callBack: PokemonListCallback) :
    RecyclerView.Adapter<DiscoveryAdapter.PokemonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val binding = ItemPokedexBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PokemonViewHolder(binding,callBack)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val currentPokemon = pokemons[position]
        holder.binding.pokedex = currentPokemon
        holder.bind(currentPokemon)
    }

    override fun getItemCount(): Int = pokemons.size

    class PokemonViewHolder(
        val binding: ItemPokedexBinding,
        val callBack: PokemonListCallback) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item : PokemonGlobalModel){
            UtilBackgroundPokemonType().setBackgroundByPokemonType(item.pokemonType, binding.pokemonCard, binding.root.context)
        }
        init {
            binding.root.setOnClickListener {
                binding.pokedex?.let { pokemon ->
                    callBack.onClick(pokemon)
                }
            }
        }
    }
}