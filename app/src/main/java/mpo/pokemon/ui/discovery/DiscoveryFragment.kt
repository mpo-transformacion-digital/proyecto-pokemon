package mpo.pokemon.ui.discovery

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import kotlinx.coroutines.launch
import mpo.pokemon.R
import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback

class DiscoveryFragment : Fragment(), PokemonListCallback {

    companion object {
        fun newInstance() = DiscoveryFragment()
    }

    private lateinit var randomeList: RecyclerView
    private lateinit var viewModel: DiscoveryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.discovery_fragment, container, false)
        randomeList = rootView.findViewById(R.id.randome_list)

        val discoveryFactory = DiscoveryViewModel.DiscoveryViewModelFactory()
        viewModel = ViewModelProvider(this, discoveryFactory)[DiscoveryViewModel::class.java]

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.view?.findViewById<ConstraintLayout>(R.id.pokelading_contraint)?.visibility = View.VISIBLE

        //Handler().postDelayed({
            viewModel = ViewModelProvider(this)[DiscoveryViewModel::class.java]
            viewModel.globalPokemonEntry.observe(viewLifecycleOwner) { randomPokemon ->
                randomeList.adapter = viewModel.globalPokemonEntry.value?.let {
                    DiscoveryAdapter(it,this)
                }
                this.view?.findViewById<ConstraintLayout>(R.id.pokelading_contraint)?.visibility = View.GONE
                this.view?.findViewById<ScrollView>(R.id.scroll_discovery)?.visibility = View.VISIBLE
            }

            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getCountPokemons();
            }
        //}, 3000)


    }

    override fun onClick(pokemon: PokemonGlobalModel) {
        val pokemonEntry = Gson().fromJson(pokemon.pokemonEntry, PokemonEntry::class.java)

        val directions = DiscoveryFragmentDirections.actionNavigationDiscoveryToPokemonDetailFragment(pokemonEntry,3)
        directions.arguments.putSerializable("pokemon_arg", pokemonEntry)
        directions.arguments.putSerializable("screen", 3)
        NavHostFragment.findNavController(this).navigate(directions)
    }

}