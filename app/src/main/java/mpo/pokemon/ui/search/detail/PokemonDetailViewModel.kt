package mpo.pokemon.ui.search.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import mpo.pokemon.data.local.`interface`.PokedexDataSource
import mpo.pokemon.data.local.db.DatabaseManager
import mpo.pokemon.data.repo.PokedexReposotiry
import mpo.pokemon.global.model.PokemonGlobalModel

class PokemonDetailViewModel(private val pokedexRepository: PokedexReposotiry) : ViewModel() {

    var pokemonGlobalModel = MutableLiveData<List<PokemonGlobalModel>?>()

    suspend fun searchInPokedex(pokemonId : String) {
        pokemonGlobalModel.value = pokedexRepository.ldbsearch(pokemonId = pokemonId)
    }

    suspend fun addToPokedex(pokemon: PokemonGlobalModel): Long {
        return pokedexRepository.add(pokemon)
    }

    suspend fun deleteFromPokedex(id: String){
        return pokedexRepository.deleteFromPokedex(id)
    }

    class PokemonDetailViewModelFactory: ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PokemonDetailViewModel::class.java)) {
                return PokemonDetailViewModel(
                    pokedexRepository = PokedexDataSource(DatabaseManager.instance.database.pokedexDao())
                ) as T
            }
            throw java.lang.Exception("Model Type not supported")
        }
    }
    // TODO: Implement the ViewModel
}