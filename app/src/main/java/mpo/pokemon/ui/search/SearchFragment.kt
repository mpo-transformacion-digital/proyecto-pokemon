package mpo.pokemon.ui.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.coroutines.launch
import mpo.pokemon.R
import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.databinding.SearchFragmentBinding
import mpo.pokemon.util.Status
import mpo.pokemon.util.UtilBackgroundPokemonType
import org.json.JSONObject


class SearchFragment : Fragment(), SearchView.OnQueryTextListener {

    companion object {
        fun newInstance() = SearchFragment()
        private var controlSearch : Int = 0
    }

    private lateinit var _binding: SearchFragmentBinding
    private lateinit var viewModel: SearchViewModel
    private lateinit var pokemonList: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val searchOptions = menu.findItem(R.id.searchmenu)
        val searchView = searchOptions.actionView as SearchView
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(this)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val factory = SearchViewModel.SearchViewModelFactory()
        viewModel = ViewModelProvider(this, factory)[SearchViewModel::class.java]
        _binding = SearchFragmentBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@SearchFragment.viewModel
        }
        pokemonList = _binding.root.findViewById(R.id.recyclerViewHistory)

        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.pokemon.observe(viewLifecycleOwner) { pokemons ->
            var adapter = pokemonList.adapter
            if (adapter == null) {
                adapter = PokemonHistoryAdapter(pokemons)
                pokemonList.adapter = adapter
            } else {
                (pokemonList.adapter as PokemonHistoryAdapter).setPokemons(pokemons)
            }
        }
        viewModel.getPokemonsHistory(_binding)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[SearchViewModel::class.java]

        // TODO: Use the ViewModel
        viewModel.pokemonLiveData.observe(viewLifecycleOwner) { result ->
            when (result?.status) {
                null -> {
                    val sharedPreference = _binding.root.context.getSharedPreferences("history-search", Context.MODE_PRIVATE)
                    var historyInfo = sharedPreference.getString("history-list","");
                        if(controlSearch==1){
                            _binding.layoutHistory.visibility = View.VISIBLE
                            _binding.layoutPokemonNoFound.visibility = View.GONE
                            controlSearch = 0
                        }else{
                            if(historyInfo!!.isNotEmpty() && controlSearch == 1){
                                _binding.layoutHistory.visibility = View.VISIBLE
                                _binding.layoutPokemonNoFound.visibility = View.GONE
                            }else{
                                _binding.layoutHistory.visibility = View.GONE
                                _binding.layoutPokemonNoFound.visibility = View.VISIBLE
                            }
                        }
                        requireView().hideKeyboard()
                }
                Status.SUCCCESS -> {
                    controlSearch = 1
                    requireView().hideKeyboard()
                    val resultPokemon = result.data
                    val pokemonCard = _binding.root.findViewById<ConstraintLayout>(R.id.pokemon_card)

                    UtilBackgroundPokemonType().setBackgroundByPokemonType(resultPokemon!!.types[0].type.name, pokemonCard, _binding.root.context)
                    addHistory(resultPokemon)

                    result.data.let {
                        val directions = it.let { it1 ->
                            SearchFragmentDirections.actionNavigationSearchToPokemonDetailFragment(it1,1)
                        }
                        NavHostFragment.findNavController(this).navigate(directions)
                    }
                }
            }
        }
    }

    private fun addHistory(resultPokemon: PokemonEntry) {
        val sharedPreference =
            _binding.root.context.getSharedPreferences("history-search", Context.MODE_PRIVATE)
        var historyInfo = sharedPreference.getString("history-list", "");
        var idAdded = sharedPreference.getString("ids-added", "");
        //sharedPreference.edit().clear().apply()

        if (!idAdded!!.contains(resultPokemon.idToString())) {

            var itemSearch = JSONObject()
            itemSearch.put("id", resultPokemon.idToString())
            itemSearch.put("name", resultPokemon.name)
            itemSearch.put("photo", resultPokemon.sprites.other!!.officialArtwork.frontDefault)
            itemSearch.put("type", resultPokemon.types[0].type.name)
            itemSearch.put("pokemon-info", resultPokemon.toString())
            var edit = sharedPreference.edit()
            edit.putString("ids-added", resultPokemon.idToString())
            if (historyInfo!!.isEmpty()) {
                historyInfo = itemSearch.toString()
                edit.putString("history-list", historyInfo).apply()
                edit.commit()
                edit.apply()
            } else {
                historyInfo = "$historyInfo,$itemSearch"
                edit.putString("history-list", historyInfo).commit()
                edit.commit()
                edit.apply()
            }
        }
    }


    private fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        if (p0 != null) {
            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getPokemonByValue(p0)
            }
        }
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (intentResult != null) {
            if (intentResult.contents == null) {
                Toast.makeText(context,"Cancel", Toast.LENGTH_SHORT).show()
            } else {
                lifecycleScope.launch {
                    viewModel.getPokemonByValue(intentResult.contents)
                }
            }
        }
    }
}