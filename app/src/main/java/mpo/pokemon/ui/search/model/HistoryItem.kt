package mpo.pokemon.ui.search.model

import java.io.Serializable

class HistoryItem(
    var id_pokemon: String,
    var pokemon_description: String,
    var num_pokemon: String,
    var url_img: String
    ) : Serializable
