package mpo.pokemon.ui.search.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.google.gson.Gson
import kotlinx.coroutines.launch
import mpo.pokemon.R
import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.databinding.PokemonDetailFragmentBinding
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.UtilBackgroundPokemonType

class PokemonDetailFragment :  DialogFragment() {

    companion object {
        fun newInstance() = PokemonDetailFragment()
    }

    private lateinit var viewModel: PokemonDetailViewModel
    private lateinit var binding: PokemonDetailFragmentBinding
    private lateinit var detailPokemon : PokemonEntry
    private var screen : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        detailPokemon = arguments?.getSerializable("pokemon_arg") as PokemonEntry
        screen = arguments?.getSerializable("screen") as Int
        binding = PokemonDetailFragmentBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            pokemon = detailPokemon
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val pokemonCard = binding.root.findViewById<ConstraintLayout>(R.id.globalPokemonDetailLayout)
        UtilBackgroundPokemonType().setBackgroundByPokemonType(detailPokemon.types[0].type.name, pokemonCard, binding.root.context)
        val factory = PokemonDetailViewModel.PokemonDetailViewModelFactory()
        viewModel = ViewModelProvider(this,factory)[PokemonDetailViewModel::class.java]

        //VALIDATE IF POKEMON EXIST IN POKEDEX
        viewModel.pokemonGlobalModel.observe(viewLifecycleOwner) { pokemon ->
            if(pokemon==null || pokemon.isEmpty()){
                if(screen==1){
                    binding.addToPokedex.visibility = View.VISIBLE
                    binding.deleteToPokedex.visibility = View.GONE
                }
                if(screen==2){
                    binding.addToPokedex.visibility = View.GONE
                    binding.deleteToPokedex.visibility = View.GONE
                }
                if(screen==3){
                    binding.addToPokedex.visibility = View.GONE
                    binding.deleteToPokedex.visibility = View.GONE
                }
            }else{
                if(screen==1){
                    binding.addToPokedex.visibility = View.GONE
                    binding.deleteToPokedex.visibility = View.GONE
                }
                if(screen==2){
                    binding.addToPokedex.visibility = View.GONE
                    binding.deleteToPokedex.visibility = View.VISIBLE
                }
                if(screen==3){
                    binding.addToPokedex.visibility = View.GONE
                    binding.deleteToPokedex.visibility = View.GONE
                }
            }
        }

        lifecycleScope.launch() {
            viewModel.searchInPokedex(detailPokemon.idToString())
        }

        binding.closeStat.setOnClickListener {
            when(screen){
                1 -> {
                    val directions = PokemonDetailFragmentDirections.actionPokemonDetailFragmentToNavigationSearch()
                    NavHostFragment.findNavController(this).navigate(directions)
                }
                2 -> {
                    val directions = PokemonDetailFragmentDirections.actionPokemonDetailFragmentToNavigationPokedex()
                    NavHostFragment.findNavController(this).navigate(directions)
                }
                3-> {
                    val directions = PokemonDetailFragmentDirections.actionPokemonDetailFragmentToNavigationDiscovery()
                    NavHostFragment.findNavController(this).navigate(directions)
                }
            }
            dismiss()
        }

        binding.addToPokedex.setOnClickListener{
            var gson = Gson()
            var jsonEntry = gson.toJson(detailPokemon)

            val pokemonGlobalModel = PokemonGlobalModel(
                detailPokemon.id.toString(),
                detailPokemon.name,
                detailPokemon.types[0].type.name,
                detailPokemon.sprites.other!!.officialArtwork.frontDefault,
                detailPokemon.stats[0].baseStat,
                detailPokemon.stats[1].baseStat,
                detailPokemon.stats[2].baseStat,
                detailPokemon.stats[3].baseStat,
                detailPokemon.stats[4].baseStat,
                detailPokemon.stats[5].baseStat,
                jsonEntry
            )


            lifecycleScope.launch() {
                val insertOperation = viewModel.addToPokedex(pokemonGlobalModel)
                if(insertOperation>0){
                    binding.addToPokedex.visibility = View.GONE
                }
            }
        }

        binding.deleteToPokedex.setOnClickListener{
            lifecycleScope.launch() {
                var deletedItems = viewModel.deleteFromPokedex(detailPokemon.id.toString())
                binding.addToPokedex.visibility = View.GONE
                binding.deleteToPokedex.visibility = View.GONE
            }
        }


    }
}