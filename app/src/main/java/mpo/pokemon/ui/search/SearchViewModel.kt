package mpo.pokemon.ui.search

import android.content.Context
import android.view.View
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import mpo.pokemon.data.remote.RetrofitBuilder
import mpo.pokemon.data.remote.model.PokemonEntry
import mpo.pokemon.data.remote.source.PokemonRemoteDataSource
import mpo.pokemon.data.repo.SearchRepository
import mpo.pokemon.databinding.SearchFragmentBinding
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.global.model.toPokemon
import mpo.pokemon.util.ObservableViewModel
import mpo.pokemon.util.Resource
import org.json.JSONArray
import java.lang.Exception

class SearchViewModel(val searchRepo: SearchRepository) :  ObservableViewModel() {

    val pokemon = MutableLiveData<List<PokemonGlobalModel>>()

    private val _pokemonLiveData : MutableLiveData<Resource<PokemonEntry>?> =
        MutableLiveData(null)
    val pokemonLiveData : LiveData<Resource<PokemonEntry>?> = _pokemonLiveData

    lateinit var bindingSearch : SearchFragmentBinding
    fun getPokemonsHistory(_binding: SearchFragmentBinding) {
        viewModelScope.launch {
            pokemon.value = getHistoryValues(_binding).value
            bindingSearch = _binding
        }
    }

    private fun getHistoryValues(_binding: SearchFragmentBinding) : LiveData<List<PokemonGlobalModel>> {
        val sharedPreference =
            _binding.root.context.getSharedPreferences("history-search", Context.MODE_PRIVATE)
        val historyInfo = sharedPreference.getString("history-list", "");

        val listMemoryPokemons = mutableListOf<PokemonGlobalModel>()
        val jsonHistory = JSONArray("[$historyInfo]")

        for (index in 0 until jsonHistory.length()) {
            val jsonPokemonHistory = jsonHistory.getJSONObject(index)
            val pokemonHistory = PokemonGlobalModel(
                jsonPokemonHistory.get("id").toString(),
                jsonPokemonHistory.get("name").toString(),
                jsonPokemonHistory.get("type").toString(),
                jsonPokemonHistory.get("photo").toString(),
                0L,
                0L,
                0L,
                0L,
                0L,
                0L,
                ""
            )
            //listMemoryPokemons.toMutableList().add(pokemonHistory)
            listMemoryPokemons.add(pokemonHistory)
        }
        return MutableLiveData(listMemoryPokemons.map { it.toPokemon() })
        //}
    }

    suspend fun getPokemonByValue(value : String) {
        try {
            val pokemonSearch = searchRepo.search(value)
            if (pokemonSearch != null) {
                _pokemonLiveData.value = Resource.success(pokemonSearch)
            }
        }
        catch (ex : Exception) {
            _pokemonLiveData.value = null //
            Resource.error(null, ex.localizedMessage)
        }
    }

    private var flagPokemonContainer : Int = 8
    var pokemonContainerVisibility : LiveData<Int> = _pokemonLiveData.switchMap { pokemonGlobalModel ->
        var visibility: Int
        if (pokemonGlobalModel == null) {
            visibility = View.GONE
            flagPokemonContainer = View.GONE
        }else{
            visibility = View.VISIBLE
            flagPokemonContainer = View.VISIBLE
        }
        MutableLiveData(visibility)
    }

    val searchContainerVisibility: LiveData<Int> =
        pokemonContainerVisibility.switchMap { pokemonContainerVisibility ->
            val visibility = if (pokemonContainerVisibility == View.GONE) {
                View.VISIBLE
            } else {
                View.GONE
            }
            MutableLiveData(visibility)
        }

    class SearchViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
                return SearchViewModel(
                    searchRepo = PokemonRemoteDataSource(RetrofitBuilder.pokemonRemoteApi)
                ) as T
            }
            throw Exception("Class type not supported.")
        }
    }

}