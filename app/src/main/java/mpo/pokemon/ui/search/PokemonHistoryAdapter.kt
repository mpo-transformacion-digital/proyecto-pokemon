package mpo.pokemon.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mpo.pokemon.databinding.ItemHistorySearchBinding
import mpo.pokemon.global.model.PokemonGlobalModel

class PokemonHistoryAdapter(
    private var pokemons: List<PokemonGlobalModel>) :
    RecyclerView.Adapter<PokemonHistoryAdapter.PokemonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val binding = ItemHistorySearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PokemonViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val currentPokemon = pokemons[position]
        holder.binding.pokemon = currentPokemon
    }

    override fun getItemCount(): Int = pokemons.size

    fun setPokemons(newPokemons: List<PokemonGlobalModel>) {
        pokemons = newPokemons
        notifyDataSetChanged()
    }

    class PokemonViewHolder(
        val binding: ItemHistorySearchBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            /*binding.root.setOnClickListener {
                binding.pokemon?.let { pokemon ->
                    callback.onClick(pokemon)
                }
            }*/
        }
    }
}