package mpo.pokemon.ui.calculator

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mpo.pokemon.databinding.ItemPokedexBinding
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback
import mpo.pokemon.util.UtilBackgroundPokemonType

class CalculatorAdapter (
    private var pokedex: List<PokemonGlobalModel>,
    private val callback: PokemonListCallback
) :
    RecyclerView.Adapter<CalculatorAdapter.PokedexViewHolder>() {

    lateinit var allPokemons : List<PokemonGlobalModel>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokedexViewHolder {
        val binding = ItemPokedexBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PokedexViewHolder(binding, callback)
    }

    var flagOne : Boolean = false
    var flagTwo : Boolean = false
    @SuppressLint("CutPasteId")
    override fun onBindViewHolder(holder: PokedexViewHolder, position: Int) {
        val currentPokemon = pokedex[position]
        holder.binding.pokedex = currentPokemon

        //holder.itemView.findViewById<ConstraintLayout>(R.id.pokemon_card).setOnClickListener{}

        holder.bind(currentPokemon)
    }

    override fun getItemCount(): Int = pokedex.size
    fun setPokemons(newPokemons: List<PokemonGlobalModel>) {
        pokedex = newPokemons
        notifyDataSetChanged()
    }

    class PokedexViewHolder(
        val binding: ItemPokedexBinding,
        private val callback: PokemonListCallback
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item : PokemonGlobalModel){
            UtilBackgroundPokemonType().setBackgroundByPokemonType(item.pokemonType, binding.pokemonCard, binding.root.context)
        }

        init {
            binding.root.setOnClickListener {
                binding.pokedex?.let { pokemon ->
                    callback.onClick(pokemon)
                }
            }
        }
    }
}