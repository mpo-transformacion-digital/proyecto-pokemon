package mpo.pokemon.ui.calculator

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import android.widget.ScrollView
import android.widget.SearchView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import mpo.pokemon.R
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback

class CalculatorFragment : Fragment(), PokemonListCallback, SearchView.OnQueryTextListener {

    private lateinit var viewModel: CalculatorViewModel
    private lateinit var pokemeterList: RecyclerView
    private lateinit var noDataPokemeter: ScrollView
    private lateinit var dataPokemeter: ScrollView
    private lateinit var removeButtonOne : Button
    private lateinit var removeButtonTwo : Button
    private lateinit var fightButton : Button
    private lateinit var selectOne : ImageView
    private lateinit var selectTwo : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val searchOptions = menu.findItem(R.id.searchmenu)
        val searchView = searchOptions.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @SuppressLint("CutPasteId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.calculator_fragment, container, false)
        pokemeterList = rootView.findViewById(R.id.recyclerViewPokemeter)
        val pokemeterFactory = CalculatorViewModel.CalculatorFactory()
        viewModel = ViewModelProvider(this, pokemeterFactory)[CalculatorViewModel::class.java]

        noDataPokemeter = rootView.findViewById(R.id.scrollNoPokemeterData)
        dataPokemeter = rootView.findViewById(R.id.scrollPokemeterData)
        removeButtonOne = rootView.findViewById(R.id.remove_one)
        removeButtonTwo = rootView.findViewById(R.id.remove_two)
        fightButton = rootView.findViewById(R.id.fight)
        selectOne = rootView.findViewById(R.id.selected_one)
        selectTwo = rootView.findViewById(R.id.selected_two)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.pokemonsPokedex.observe(viewLifecycleOwner) { pokemons ->
            var adapter = pokemeterList.adapter
            if (adapter == null) {
                adapter = CalculatorAdapter(pokemons, this)
                pokemeterList.adapter = adapter
                if(pokemons.isNotEmpty()){
                    noDataPokemeter.visibility = View.GONE
                    dataPokemeter.visibility = View.VISIBLE
                    (pokemeterList.adapter as CalculatorAdapter).setPokemons(pokemons)
                }else{
                    noDataPokemeter.visibility = View.VISIBLE
                    dataPokemeter.visibility = View.GONE
                }
            } else {
                (pokemeterList.adapter as CalculatorAdapter).setPokemons(pokemons)
            }
        }

        removeButtonOne.setOnClickListener {
            selectOne.setImageResource(R.drawable.ic_outline_help_center)
            removeButtonOne.visibility = View.GONE
            fightButton.visibility = View.GONE
            selectedPokemons.removeAt(0)
        }

        removeButtonTwo.setOnClickListener {
            selectTwo.setImageResource(R.drawable.ic_outline_help_center)
            removeButtonTwo.visibility = View.GONE
            fightButton.visibility = View.GONE
            selectedPokemons.removeAt(1)
        }

        fightButton.setOnClickListener{
            val directions = CalculatorFragmentDirections.actionNavigationCalculatorToCalculatorDetailFragment(
                selectedPokemons[0],selectedPokemons[1])
            directions.arguments.putSerializable("pokemon_one", selectedPokemons[0])
            directions.arguments.putSerializable("pokemon_two", selectedPokemons[1])
            NavHostFragment.findNavController(this).navigate(directions)
        }

        viewModel.getAllPokemosFromDb()
    }


    private var selectedPokemons = ArrayList<PokemonGlobalModel>()
    override fun onClick(pokemon: PokemonGlobalModel) {

        do {
            if(removeButtonOne.visibility==View.GONE){
                Picasso.get().load(pokemon.pokemonImage).into(selectOne)
                removeButtonOne.visibility = View.VISIBLE
                selectedPokemons.add(0 , pokemon)
                if(removeButtonTwo.visibility==View.VISIBLE && removeButtonOne.visibility==View.VISIBLE){
                    fightButton.visibility = View.VISIBLE
                    break
                }
                break
            }

            if(removeButtonTwo.visibility==View.GONE && removeButtonOne.visibility==View.VISIBLE){
                Picasso.get().load(pokemon.pokemonImage).into(selectTwo)
                removeButtonTwo.visibility = View.VISIBLE
                selectedPokemons.add(1 , pokemon)
                if(removeButtonTwo.visibility==View.VISIBLE && removeButtonOne.visibility==View.VISIBLE){
                    fightButton.visibility = View.VISIBLE
                    break
                }
                break
            }

        }while (true)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(filter: String?): Boolean {
        if(filter!!.isNotEmpty()){
            lifecycleScope.launch {
                viewModel.getPokemonFromDb(filter)
                pokemeterList.adapter = null
            }
        }else{
            lifecycleScope.launch {
                viewModel.getAllPokemosFromDb()
                pokemeterList.adapter = null
            }
        }
        return false
    }

}