package mpo.pokemon.ui.calculator

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mpo.pokemon.data.local.`interface`.PokedexDataSource
import mpo.pokemon.data.local.db.DatabaseManager
import mpo.pokemon.data.repo.PokedexReposotiry
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.ObservableViewModel
import java.lang.Exception

class CalculatorViewModel (private val pokedexRepository: PokedexReposotiry) : ObservableViewModel() {
    val pokemonsPokedex = MutableLiveData<List<PokemonGlobalModel>>()

    fun getAllPokemosFromDb() {
        viewModelScope.launch {
            pokemonsPokedex.value = pokedexRepository.getAllPokemonsFromPokedex().value
        }
    }

    fun getPokemonFromDb(query : String) {
        viewModelScope.launch {
            pokemonsPokedex.value = pokedexRepository.getPokemonsFromPokedexDb("%${query}%").value
        }
    }

    class CalculatorFactory: ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CalculatorViewModel::class.java)){
                return CalculatorViewModel(
                    pokedexRepository = PokedexDataSource(DatabaseManager.instance.database.pokedexDao())
                ) as T
            }
            throw Exception("No class type supported")
        }
    }
}