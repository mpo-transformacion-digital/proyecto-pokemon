package mpo.pokemon.ui.calculator.detail

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import mpo.pokemon.R
import mpo.pokemon.global.model.PokemonGlobalModel
import mpo.pokemon.util.PokemonListCallback
import mpo.pokemon.util.UtilCalculator
import org.json.JSONObject
import pl.droidsonroids.gif.GifImageView
import java.util.*

class CalculatorDetailFragment : Fragment(), PokemonListCallback {

    companion object {
        fun newInstance() = CalculatorDetailFragment()
    }

    private lateinit var viewModel: CalculatorDetailViewModel
    private lateinit var pokemonOne : PokemonGlobalModel
    private lateinit var pokemonTwo : PokemonGlobalModel

    //Components
    private lateinit var levelValueOne : TextView
    private lateinit var powerValueOne : TextView
    private lateinit var levelValueTwo : TextView
    private lateinit var powerValueTwo : TextView

    private lateinit var damage : TextView
    private lateinit var right : ImageView
    private lateinit var left : ImageView
    private lateinit var pokeballLoading : GifImageView

    private lateinit var hitOne : Button
    private lateinit var hitTwo : Button


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        pokemonOne = arguments?.getSerializable("pokemon_one") as PokemonGlobalModel
        pokemonTwo = arguments?.getSerializable("pokemon_two") as PokemonGlobalModel
        val pokemeterDetailFactory = CalculatorDetailViewModel.CalculatorDetailFactory()
        viewModel = ViewModelProvider(this, pokemeterDetailFactory)[CalculatorDetailViewModel::class.java]
        val rootView = inflater.inflate(R.layout.calculator_detail_fragment, container, false)

        //POKEMON ONE
        rootView.findViewById<TextView>(R.id.pokemon_one).text = pokemonOne.pokemonName.uppercase(Locale.getDefault())
        val imgView = rootView.findViewById<ImageView>(R.id.selected_one_detail)
        Picasso.get().load(pokemonOne.pokemonImage).into(imgView)
        rootView.findViewById<TextView>(R.id.hp_value_one).text = pokemonOne.hp.toString()
        rootView.findViewById<TextView>(R.id.attack_value_one).text = pokemonOne.attack.toString()
        rootView.findViewById<TextView>(R.id.defense_value).text = pokemonOne.defense.toString()

        //POKEMON TWO
        rootView.findViewById<TextView>(R.id.pokemon_two).text = pokemonTwo.pokemonName.uppercase(Locale.getDefault())
        val imgViewTwo = rootView.findViewById<ImageView>(R.id.selected_two_detail)
        Picasso.get().load(pokemonTwo.pokemonImage).into(imgViewTwo)
        rootView.findViewById<TextView>(R.id.hp_value_two).text = pokemonTwo.hp.toString()
        rootView.findViewById<TextView>(R.id.attack_value_two).text = pokemonTwo.attack.toString()
        rootView.findViewById<TextView>(R.id.defense_value_two).text = pokemonTwo.defense.toString()

        hitOne = rootView.findViewById(R.id.hit_one)
        hitTwo = rootView.findViewById(R.id.hit_two)

        levelValueOne = rootView.findViewById(R.id.level_value_one)
        powerValueOne = rootView.findViewById(R.id.power_value_one)
        levelValueTwo = rootView.findViewById(R.id.level_value_two)
        powerValueTwo = rootView.findViewById(R.id.power_value_two)
        damage = rootView.findViewById(R.id.damage)
        right = rootView.findViewById(R.id.right)
        left = rootView.findViewById(R.id.left)
        pokeballLoading = rootView.findViewById(R.id.pokeball_loading_calc)

        return rootView
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var buttonPress = 0
        hitOne.setOnClickListener {
            pokeballLoading.visibility = View.VISIBLE
            lifecycleScope.launch {
                buttonPress = 1
                viewModel.getPokemonEffectivenes(pokemonOne.pokemonType, pokemonTwo.pokemonType)
            }
        }

        hitTwo.setOnClickListener {
            pokeballLoading.visibility = View.VISIBLE
            lifecycleScope.launch {
                buttonPress = 2
                viewModel.getPokemonEffectivenes(pokemonOne.pokemonType, pokemonTwo.pokemonType)
            }
        }

        viewModel.awsReponse.observe(viewLifecycleOwner){ response ->
            var jsonResponse = JSONObject(response.response)
            val randomLevel = UtilCalculator().getRandomeLevel()
            val randomPower = UtilCalculator().getRandomePower()

            when(buttonPress){
                1 -> {
                    var damageValueOne = UtilCalculator().pokemonDamageFormula(pokemonOne,pokemonTwo,jsonResponse.getDouble("pokemon_one").toFloat(), randomLevel, randomPower)
                    levelValueOne.text = randomLevel.toString()
                    powerValueOne.text = randomPower.toString()
                    damageValueOne = String.format("%.2f", damageValueOne).toFloat()
                    damage.text = "$damageValueOne DMG"
                    right.visibility = View.VISIBLE
                    left.visibility = View.GONE
                    pokeballLoading.visibility = View.GONE
                }
                2 -> {
                    var damageValueTwo = UtilCalculator().pokemonDamageFormula(pokemonTwo,pokemonOne,jsonResponse.getDouble("pokemon_two").toFloat(), randomLevel, randomPower)
                    levelValueTwo.text = randomLevel.toString()
                    powerValueTwo.text = randomPower.toString()
                    damageValueTwo = String.format("%.2f", damageValueTwo).toFloat()
                    damage.text = "$damageValueTwo DMG"
                    right.visibility = View.GONE
                    left.visibility = View.VISIBLE
                    pokeballLoading.visibility = View.GONE
                }
            }
        }
    }

    override fun onClick(pokemon: PokemonGlobalModel) {
        TODO("Not yet implemented")
    }

}