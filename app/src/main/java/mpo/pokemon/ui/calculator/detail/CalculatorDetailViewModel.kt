package mpo.pokemon.ui.calculator.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import mpo.pokemon.data.remote.RetrofitBuilder
import mpo.pokemon.data.remote.model.AwsRespondeEntity
import mpo.pokemon.data.remote.source.PokemonAwsRemoteDataSource
import mpo.pokemon.data.repo.CalculatorRepository
import mpo.pokemon.util.ObservableViewModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.lang.Exception

class CalculatorDetailViewModel (private val pokemonAwsRepository : CalculatorRepository) : ObservableViewModel() {
    val awsReponse = MutableLiveData<AwsRespondeEntity>()

    suspend fun getPokemonEffectivenes(typeOne : String, typeTwo: String) {
        val jsonObject = JSONObject()
        jsonObject.put("type_one", typeOne)
        jsonObject.put("type_two", typeTwo)

        val jsonString = jsonObject.toString()

        val requestBody = jsonString.toRequestBody("application/json".toMediaTypeOrNull())
        awsReponse.value = pokemonAwsRepository.getPokemonEffectiveness(requestBody)
    }

    class CalculatorDetailFactory: ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CalculatorDetailViewModel::class.java)){
                return CalculatorDetailViewModel(
                    pokemonAwsRepository = PokemonAwsRemoteDataSource(RetrofitBuilder.pokemonAWSRemoteApi)
                ) as T
            }
            throw Exception("No class type supported")
        }
    }
}