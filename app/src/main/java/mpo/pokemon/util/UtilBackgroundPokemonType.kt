package mpo.pokemon.util

import android.content.Context
import android.os.Build
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import mpo.pokemon.R
import kotlin.properties.Delegates

class UtilBackgroundPokemonType {
    private var background by Delegates.notNull<Int>()
    fun setBackgroundByPokemonType(
        resultPokemon: String,
        pokemonCard: ConstraintLayout, context : Context
    ) {

        when (resultPokemon) {
            "electric" -> background = R.drawable.edit_text_type_element_electric
            "normal" -> background = R.drawable.edit_text_type_element_normal
            "fire" -> background = R.drawable.edit_text_type_element_fire
            "water" -> background = R.drawable.edit_text_type_element_water
            "grass" -> background = R.drawable.edit_text_type_element_grass
            "ice" -> background = R.drawable.edit_text_type_element_ice
            "fighting" -> background = R.drawable.edit_text_type_element_fighting
            "poison" -> background = R.drawable.edit_text_type_element_poison
            "ground" -> background = R.drawable.edit_text_type_element_ground
            "flying" -> background = R.drawable.edit_text_type_element_flying
            "psychic" -> background = R.drawable.edit_text_type_element_psychic
            "bug" -> background = R.drawable.edit_text_type_element_bug
            "rock" -> background = R.drawable.edit_text_type_element_rock
            "ghost" -> background = R.drawable.edit_text_type_element_ghost
            "dragon" -> background = R.drawable.edit_text_type_element_dragon
            "dark" -> background = R.drawable.edit_text_type_element_dark
            "steel" -> background = R.drawable.edit_text_type_element_steel
            "fairy" -> background = R.drawable.edit_text_type_element_fairy
        }

        val sdk = Build.VERSION.SDK_INT;
        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
            pokemonCard.setBackgroundDrawable(
                ContextCompat.getDrawable(context, background)
            );
        } else {
            pokemonCard.background =
                ContextCompat.getDrawable(context, background);
        }

    }
}