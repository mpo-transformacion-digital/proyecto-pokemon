package mpo.pokemon.util

import mpo.pokemon.global.model.PokemonGlobalModel
import kotlin.random.Random

class UtilCalculator {

    fun getRandomeLevel() : Int {
        return Random.nextInt(1, 10)
    }

    fun getRandomePower() : Int {
        return Random.nextInt(1, 50)
    }

    fun pokemonDamageFormula(
        pokemonOne: PokemonGlobalModel,
        pokemonTwo: PokemonGlobalModel,
        effectiveness: Float,
        level: Int,
        power: Int
    ): Float {
        //((2 * level)/5)+2
        val formulaPartOne = (((2 * level.toFloat()) / 5) + 2)
        //(Power * (A/D)
        val formulaPartTwo = power.toFloat() * (pokemonOne.attack.toFloat() / pokemonTwo.defense.toFloat())
        //((formulaPartOne * formulaPartTwo/50)+2) * type
        return (((formulaPartOne * formulaPartTwo) / 50) + 2) * effectiveness
    }
}