package mpo.pokemon.util

import mpo.pokemon.global.model.PokemonGlobalModel

interface PokemonListCallback {
    fun onClick(pokemon: PokemonGlobalModel)
}